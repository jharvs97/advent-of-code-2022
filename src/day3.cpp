#include "aoc.cpp"

s32 get_priority_for_item(char c)
{
    if (c >= 'A' && c <= 'Z')
    {
        return ((c - 'A') + 1) + 26;
    }
    else if (c >= 'a' && c <= 'z')
    {
        return (c - 'a') + 1;
    }
    
    return 0;
}

u64 get_priority_bitmask(char c)
{
    return 1ull << (((u64) get_priority_for_item(c)) - 1);
}

char get_item_from_bitmask(u64 bitfield)
{
    unsigned long num_zeros;
    // NOTE: Windows specific intrinsic, use something like '__builtin_ctz' with GCC/Clang
    _BitScanReverse64(&num_zeros, bitfield);
    
    if (num_zeros < 26)
    {
        return 'a' + num_zeros;
    }
    else
    {
        return 'A' + (num_zeros - 26);
    }
}

u64 get_items_bitfield(String_View sv)
{
    u64 result = 0;
    for (char* c = sv.begin;
         c <= sv.end;
         c++)
    {
        result |= get_priority_bitmask(*c);
    }
    
    return result;
}

PART1()
{
    String_View file_contents = read_entire_file("../data/day3.txt", &arena);
    
    String_View line;
    
    int sum_of_priorities = 0;
    
    while (read_line(&file_contents, &line))
    {
        usize length = string_view_length(line);
        // string_view_length is not 0 index based, so subtract one.
        usize half_length = (length / 2) - 1;
        
        String_View first_half = string_view_pop_n(&line, half_length);
        String_View second_half = line;
        
        u64 first_bitfield = get_items_bitfield(first_half);
        u64 second_bitfield = get_items_bitfield(second_half);
        
        u64 duplicate_bitmask = first_bitfield & second_bitfield;
        char item = get_item_from_bitmask(duplicate_bitmask);
        
        sum_of_priorities += get_priority_for_item(item);
    }
    
    printf("%d\n", sum_of_priorities);
}

PART2()
{
    String_View file_contents = read_entire_file("../data/day3.txt", &arena);
    
    String_View line;
    
    s32 total_priority = 0;
    
    while (string_view_length(file_contents) > 0)
    {
        String_View line1, line2, line3;
        
        read_line(&file_contents, &line1);
        read_line(&file_contents, &line2);
        read_line(&file_contents, &line3);
        
        u64 elf1 = get_items_bitfield(line1);
        u64 elf2 = get_items_bitfield(line2);
        u64 elf3 = get_items_bitfield(line3);
        
        u64 common_bitfield = elf1 & elf2 & elf3;
        
        char common_item = get_item_from_bitmask(common_bitfield);
        s32 priority = get_priority_for_item(common_item);
        
        total_priority += priority;
    }
    
    printf("%d\n", total_priority);
}