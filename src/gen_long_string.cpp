#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

#define SIZE 10000000
#define UNIQUE_SIZE 23

int main()
{
    char* buffer = (char*) malloc(SIZE + UNIQUE_SIZE);
    srand((unsigned int) time(0));
    
    for (size_t i = 0;
         i < SIZE;
         i++)
    {
        int normalized = rand() % 25;
        char c = ((char) normalized) + 'a';
        buffer[i] = c;
    }
    
    for (size_t i = 0;
         i < UNIQUE_SIZE;
         i++)
    {
        buffer[SIZE + i] = 'a' + (char) i;
    }
    
    FILE* file;
    errno_t error = fopen_s(&file, "../data/long.txt", "w");
    
    if (error == 0)
    {
        fwrite(buffer, sizeof(char), SIZE + UNIQUE_SIZE, file);
    }
}