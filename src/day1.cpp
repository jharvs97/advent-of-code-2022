#include "aoc.cpp"

#define SCRATCH_BUF_LEN 512
#define LARGEST_VALUES_BUFFER_SIZE 3

// Buffer which tracks N largest values, sorted. 
// 0th elem is lowest, N-1 elem is largest
struct Largest_Values
{
    int stack[LARGEST_VALUES_BUFFER_SIZE];
};

void try_insert(Largest_Values* values, int num)
{
    for (size_t i = 0; i < LARGEST_VALUES_BUFFER_SIZE; i++)
    {
        if (num > values->stack[i])
        {
            for (size_t j = 0; j < i; j++)
            {
                values->stack[j] = values->stack[j+1];
            }
            
            values->stack[i] = num;
        }
    }
}

int sum(Largest_Values values)
{
    int total = 0;
    for (size_t i = 0; i < LARGEST_VALUES_BUFFER_SIZE; i++)
    {
        total += values.stack[i];
    }
    
    return total;
}

PART1()
{
    String_View file_contents = read_entire_file("../data/day1.txt", &arena);
    
    String_View line;
    
    int running_calories = 0;
    int highest_calories = 0;
    
    while (read_line(&file_contents, &line))
    {
        if (string_view_length(line) == 0)
        {
            if (running_calories > highest_calories)
            {
                highest_calories = running_calories;
            }
            running_calories = 0;
        }
        else
        {
            int calories = string_view_to_int(line, arena);
            running_calories += calories;
        }
    }
    
    printf("%d\n", highest_calories);
}


PART2()
{
    String_View file_contents = read_entire_file("../data/day1.txt", &arena);
    
    String_View line;
    
    int running_calories = 0;
    Largest_Values highest_calories = {};
    
    while (read_line(&file_contents, &line))
    {
        if (string_view_length(line) == 0)
        {
            try_insert(&highest_calories, running_calories);
            running_calories = 0;
        }
        else
        {
            int calories = string_view_to_int(line, arena);
            running_calories += calories;
        }
    }
    
    printf("%d\n", sum(highest_calories));
}