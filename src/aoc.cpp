#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>
#include <intrin.h>
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <pathcch.h>

typedef size_t usize;

typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef u8 byte;

#define U64_MAX 18446744073709551615ull

#include "memory_arena.cpp"
#include "string_view.cpp"

#ifdef BUILD_DLL
#define JRYNS_API __declspec(dllexport)
#else
#define JRYNS_API __declspec(dllimport)
#endif

#define AOC_FUNC(name) JRYNS_API void name(Memory_Arena arena)
typedef AOC_FUNC(aoc_func);

#define PART1_NAME part1
#define PART2_NAME part2

#define TO_STRING(s) #s
#define MACRO_TO_STRING(s) TO_STRING(s)

#define PART1() extern "C" AOC_FUNC(PART1_NAME)
#define PART2() extern "C" AOC_FUNC(PART2_NAME)