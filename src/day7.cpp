#include "aoc.cpp"

typedef String_View Token;

#define MAX_TOKENS 8
struct Token_Array
{
    Token token_buffer[MAX_TOKENS];
    usize head;
};

#define MAX_LINES 4096
struct Tokenized_Lines
{
    Token_Array* line_buffer;
    usize num_lines;
};

enum Command_Type
{
    Command_cd,
    Command_ls,
};

struct Command
{
    Command_Type type;
    String_View dest_dir_name;
};

enum File_Type
{
    File_file,
    File_dir,
};

// Files and Dirs are the same
struct File
{
    usize size_on_disk;
    String_View name;
    File_Type type;
};

enum Parsed_Token_Type
{
    Parsed_Token_command,
    Parsed_Token_file,
};

struct Parsed_Token_Output
{
    Parsed_Token_Type type;
    union {
        Command command;
        File file;
    };
};

#define MAX_PARSED_TOKENS 2048
struct Parsed_Tokens
{
    Parsed_Token_Output* buffer;
    usize head;
};

#define MAX_CHILDREN 16
struct File_Node
{
    File current;
    
    File_Node* parent;
    File_Node* children[MAX_CHILDREN];
    usize head;
};

static File_Node nil_file_node;

enum Eval_State
{
    Eval_State_do_command,
    Eval_State_create_files,
};

void tokenize_line(Token_Array* tokens, String_View line)
{
    while (!string_view_is_empty(line))
    {
        string_view_trim_front(&line);
        String_View tok_str = string_view_split(&line, ' ');
        
        tokens->token_buffer[tokens->head++] = tok_str;
    }
}

Parsed_Token_Output parse_command(Token_Array tokens)
{
    Parsed_Token_Output result = {};
    result.type = Parsed_Token_command;
    
    Command command = {};
    
    if (string_view_eq(tokens.token_buffer[1], "cd"_sv))
    {
        command.type = Command_cd;
        command.dest_dir_name = tokens.token_buffer[2];
    }
    else if (string_view_eq(tokens.token_buffer[1], "ls"_sv))
    {
        command.type = Command_ls;
    }
    
    result.command = command;
    
    return result;
}

Parsed_Token_Output parse_file(Token_Array tokens, Memory_Arena arena)
{
    Parsed_Token_Output result = {};
    result.type = Parsed_Token_file;
    
    File file = {};
    
    if (string_view_eq(tokens.token_buffer[0], "dir"_sv))
    {
        file.type = File_dir;
        file.name = tokens.token_buffer[1];
    }
    else
    {
        file.type = File_file;
        file.size_on_disk = string_view_to_int(tokens.token_buffer[0], arena);
        file.name = tokens.token_buffer[1];
    }
    
    result.file = file;
    
    return result;
}

Parsed_Token_Output parse_line(Token_Array tokens, Memory_Arena arena)
{
    Parsed_Token_Output output = {};
    if (string_view_eq(tokens.token_buffer[0], "$"_sv))
    {
        output = parse_command(tokens);
    }
    else
    {
        output = parse_file(tokens, arena);
    }
    
    return output;
}

Parsed_Tokens parse_tokens(Tokenized_Lines lines, Memory_Arena* arena)
{
    Parsed_Tokens parsed_tokens = {};
    parsed_tokens.buffer = arena_push_array(arena, Parsed_Token_Output, MAX_PARSED_TOKENS);
    
    for (usize i = 0;
         i < lines.num_lines;
         i++)
    {
        Token_Array tokens = lines.line_buffer[i];
        Parsed_Token_Output output = parse_line(tokens, *arena);
        parsed_tokens.buffer[parsed_tokens.head++] = output;
    }
    
    return parsed_tokens;
}

File_Node* make_directory(Memory_Arena* arena, File_Node* parent, String_View name)
{
    File_Node* new_dir = arena_push_struct(arena, File_Node);
    new_dir->current.name = name;
    new_dir->current.type = File_dir;
    new_dir->current.size_on_disk = 0;
    new_dir->head = 0;
    new_dir->parent = parent;
    
    if (parent != &nil_file_node)
    {
        parent->children[parent->head++] = new_dir;
    }
    
    return new_dir;
}

File_Node* change_directory(String_View dir_name, File_Node* current)
{
    
    if (string_view_eq(dir_name, ".."_sv))
    {
        return current->parent;
    }
    else if (string_view_eq(dir_name, "/"_sv))
    {
        File_Node* start = current;
        while (start->parent != &nil_file_node)
        {
            start = start->parent;
        }
        return start;
    }
    else
    {
        for (usize i = 0;
             i < current->head;
             i++)
        {
            File_Node* child = current->children[i];
            
            if (child->current.type == File_dir &&
                string_view_eq(child->current.name, dir_name))
            {
                return child;
            }
        }
    }
    return &nil_file_node;
}

Eval_State do_command(Command command, File_Node** current_node)
{
    switch (command.type)
    {
        case Command_cd:
        {
            File_Node* cd_target = change_directory(command.dest_dir_name, *current_node);
            if (cd_target == &nil_file_node)
            {
                printf("Directory doesn't exist\n");
            }
            else
            {
                *current_node = cd_target;
            }
            return Eval_State_do_command;
        } break;
        case Command_ls:
        {
            return Eval_State_create_files;
        } break;
    }
    
    assert(false);
    return (Eval_State)0;
}

Eval_State create_file_in_tree(File file, File_Node* current_node, Memory_Arena* arena)
{
    File_Node** where_to_insert = &(current_node->children[current_node->head++]);
    *where_to_insert = arena_push_struct(arena, File_Node);
    (*where_to_insert)->current = file;
    (*where_to_insert)->parent = current_node;
    (*where_to_insert)->head = 0;
    
    return Eval_State_create_files;
}

File_Node* build_initial_tree(Parsed_Tokens parsed, Memory_Arena* arena)
{
    File_Node* tree = make_directory(arena, &nil_file_node, "/"_sv);
    File_Node* current_node = tree;
    Eval_State state = Eval_State_do_command;
    
    usize i = 0;
    while (i < parsed.head)
    {
        Parsed_Token_Output current = parsed.buffer[i];
        
        switch (state)
        {
            case Eval_State_do_command:
            {
                state = do_command(current.command, &current_node);
                i++;
            } break;
            case Eval_State_create_files:
            {
                if (current.type == Parsed_Token_file)
                {
                    state = create_file_in_tree(current.file, current_node, arena);
                    i++;
                }
                else
                {
                    state = Eval_State_do_command;
                }
            } break;
        }
    }
    
    return tree;
}

usize recursive_calculate_dir_sizes(File_Node* tree)
{
    if (tree->current.type == File_file)
    {
        return tree->current.size_on_disk;
    }
    
    for (int i = 0;
         i < tree->head;
         i++)
    {
        tree->current.size_on_disk += recursive_calculate_dir_sizes(tree->children[i]);
    }
    
    return tree->current.size_on_disk;
}

File_Node* solve_directory_tree_from_input(String_View* input, Memory_Arena* arena)
{
    String_View line;
    
    Tokenized_Lines lines = {};
    lines.line_buffer = arena_push_array(arena, Token_Array, MAX_LINES);
    
    // First parse. get the tokens
    while (read_line(input, &line))
    {
        tokenize_line(&lines.line_buffer[lines.num_lines++], line);
    }
    
    Parsed_Tokens parsed = parse_tokens(lines, arena);
    File_Node* tree = build_initial_tree(parsed, arena);
    
    recursive_calculate_dir_sizes(tree);
    
    return tree;
}

usize sum_all_directory_sizes_with_size_less_then_n_inclusive(File_Node* tree, usize max_size)
{
    usize total = 0;
    
    if (tree->current.type == File_dir)
    {
        // Add this dir
        if (tree->current.size_on_disk <= max_size)
        {
            total += tree->current.size_on_disk;
        }
        
        // Add all the children
        for (usize i = 0;
             i < tree->head;
             i++)
        {
            total += sum_all_directory_sizes_with_size_less_then_n_inclusive(tree->children[i], max_size);
        }
    }
    
    return total;
}

usize find_directory_with_minimum_size(File_Node* tree, usize minimum_size)
{
    if (tree->current.type == File_dir)
    {
        if (tree->current.size_on_disk >= minimum_size)
        {
            usize best = tree->current.size_on_disk;
            for (usize i = 0;
                 i < tree->head;
                 i++)
            {
                usize sub_dir_size = find_directory_with_minimum_size(tree->children[i], minimum_size);
                
                if (sub_dir_size >= minimum_size && sub_dir_size < best)
                {
                    best = sub_dir_size;
                }
            }
            
            return best;
        }
        else
        {
            return tree->current.size_on_disk;
        }
    }
    
    return 0;
}

PART1()
{
    String_View file_contents = read_entire_file("../data/day7.txt", &arena);
    
    File_Node* tree = solve_directory_tree_from_input(&file_contents, &arena);
    usize part1 = sum_all_directory_sizes_with_size_less_then_n_inclusive(tree, 100000);
    
    printf("%llu\n", part1);
}

PART2()
{
    String_View file_contents = read_entire_file("../data/day7.txt", &arena);
    
    File_Node* tree = solve_directory_tree_from_input(&file_contents, &arena);
    usize total_space = 70000000;
    usize space_required = 30000000;
    
    usize remaining_space = total_space - tree->current.size_on_disk;
    usize amount_to_free = space_required;
    if (remaining_space < space_required)
    {
        amount_to_free = space_required - remaining_space;
    }
    
    usize part2 = find_directory_with_minimum_size(tree, amount_to_free);
    
    printf("%llu\n", part2);
}