struct Memory_Arena
{
    byte* memory;
    usize capacity;
    usize bytes_written;
};

Memory_Arena arena_create(usize size_in_bytes)
{
    Memory_Arena arena = {};
    arena.memory = (byte*) malloc(size_in_bytes);
    arena.bytes_written = 0;
    arena.capacity = size_in_bytes;
    
    return arena;
}

void arena_destroy(Memory_Arena* arena)
{
    free(arena->memory);
    arena->bytes_written = 0;
}

byte* arena_allocate(Memory_Arena* arena, usize size_in_bytes)
{
    if ((arena->memory + (arena->bytes_written + size_in_bytes)) > (arena->memory + arena->capacity))
    {
        return 0;
    }
    byte* address = arena->memory + arena->bytes_written;
    arena->bytes_written += size_in_bytes;
    return address;
}

#define arena_push_array(arena, T, size) (T*) arena_allocate(arena, (sizeof(T) * (size)))
#define arena_push_struct(arena, T) (T*) arena_allocate(arena, sizeof(T))

void arena_reset(Memory_Arena* arena)
{
    arena->bytes_written = 0;
}