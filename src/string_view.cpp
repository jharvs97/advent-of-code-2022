struct String_View
{
    char* begin = 0;
    char* end   = 0;
};

String_View operator "" _sv(const char* c_str, usize size)
{
    return {(char *)c_str, (char *) c_str + (size-1)};
}

String_View read_entire_file(const char* file_name, Memory_Arena* arena)
{
    String_View string = {};
    
    FILE* file;
    errno_t err = fopen_s(&file, file_name, "r");
    
    if (err == 0)
    {
        // Find the length of the file
        fseek(file, 0, SEEK_END);
        size_t length = ftell(file);
        fseek(file, 0, SEEK_SET);
        
        // Allocate (length + 1) bytes to store the null character
        size_t buffer_size = length + 1;
        char* buffer = (char*) arena_allocate(arena, buffer_size);
        
        // read in length characters from the file
        size_t read = fread_s(buffer, buffer_size, sizeof(char), length, file);
        
        // Ensure that the length of the returned string matches the bytes we read in
        length = read;
        
        string.begin = buffer;
        string.end = buffer + length;
    }
    
    return string;
}

size_t string_view_length(String_View sv)
{
    return (sv.end - sv.begin) + 1;
}

bool string_view_is_empty(String_View sv)
{
    return (sv.end - sv.begin) < 0 || (*sv.begin == 0 && *sv.end == 0);
}

void print_string_view(String_View sv)
{
    fwrite(sv.begin, sizeof(char), string_view_length(sv), stdout);
}

bool read_line(String_View* in, String_View* out)
{
    if (string_view_is_empty(*in))
        return false;
    
    out->begin = in->begin;
    size_t out_length = 0;
    
    for (char* c = in->begin;
         *c != '\n' && c != in->end;
         c++)
    {
        out_length++;
    }
    
    if (out_length == 0)
    {
        out->end = out->begin;
        out->begin += 1;
    }
    else
    {
        out->end = out->begin + (out_length - 1);
    }
    
    in->begin += out_length + 1;
    if (in->begin > in->end)
    {
        in->begin = in->end;
    }
    
    return true;
}

char* copy_string_view_to_null_terminating_buffer(String_View sv, Memory_Arena* arena)
{
    usize length = string_view_length(sv);
    
    char* buffer = (char*) arena_allocate(arena, length + 1);
    
    memcpy(buffer, sv.begin, length);
    
    buffer[length] = 0;
    
    return buffer;
}

s32 string_view_to_int(String_View sv, Memory_Arena arena)
{
    char* num_buffer = copy_string_view_to_null_terminating_buffer(sv, &arena);
    return atoi(num_buffer);
}

String_View string_view_pop_n(String_View* sv, usize n)
{
    // TODO: Not doing any bounds checking here.
    String_View result = {sv->begin, sv->begin + n};
    sv->begin += (n + 1);
    
    return result;
}

String_View string_view_split(String_View* sv, char delimiter)
{
    usize count = 0;
    for (char* c = sv->begin;
         c <= sv->end;
         c++)
    {
        if (*c == delimiter)
        {
            break;
        }
        count++;
    }
    
    // NOTE: Ensure we don't include the delimiter
    String_View result = {sv->begin, sv->begin + (count - 1)};
    sv->begin += (count + 1);
    
    return result;
}

void string_view_trim_front(String_View* sv)
{
    for (char* c = sv->begin;
         c <= sv->end;)
    {
        if (*c == ' ')
        {
            sv->begin++;
        }
        else
        {
            break;
        }
    }
}

bool string_view_eq(String_View a, String_View b)
{
    
    if (string_view_length(a) != string_view_length(b))
    {
        return false;
    }
    
    if (a.begin == b.begin && a.end == b.end)
    {
        return true;
    }
    
    for (char *aa = a.begin, *bb = b.begin;
         aa < a.end;
         aa++, bb++)
    {
        if (*aa != *bb)
        {
            return false;
        }
    }
    
    return true;
}