#include "aoc.cpp"

enum Move
{
    Move_invalid = 0,
    Move_rock = 1,
    Move_paper = 2,
    Move_scissors = 3,
    
};

enum Outcome
{
    Outcome_lose = 0,
    Outcome_draw = 3,
    Outcome_win = 6,
};

Move get_move(char move)
{
    switch (move)
    {
        case 'A': 
        case 'X': 
        {
            return Move_rock;
        } break;
        case 'B': 
        case 'Y':
        {
            return Move_paper;
        } break;
        case 'C': 
        case 'Z':
        {
            return Move_scissors;
        } break;
        default: return Move_invalid;
    }
}

int evaluate(Move mine, Move opponent)
{
    assert(mine != Move_invalid);
    assert(opponent != Move_invalid);
    
    Outcome outcome = Outcome_lose;
    if (mine == opponent)
    {
        outcome = Outcome_draw;
    }
    else if (mine == Move_rock)
    {
        switch (opponent)
        {
            case Move_scissors: outcome = Outcome_win; break;
            case Move_paper: outcome = Outcome_lose; break;
        }
    }
    else if (mine == Move_paper)
    {
        switch (opponent)
        {
            case Move_rock: outcome = Outcome_win; break;
            case Move_scissors: outcome = Outcome_lose; break;
        }
    }
    else if (mine == Move_scissors)
    {
        switch (opponent)
        {
            case Move_paper: outcome = Outcome_win; break;
            case Move_rock: outcome = Outcome_lose; break;
        }
    }
    
    return outcome + mine;
}

enum Desired_Outcome
{
    Desired_Outcome_invalid,
    Desired_Outcome_win,
    Desired_Outcome_lose,
    Desired_Outcome_draw,
};


Desired_Outcome get_desired_outcome(char c)
{
    switch (c)
    {
        case 'X': return Desired_Outcome_lose;
        case 'Y': return Desired_Outcome_draw;
        case 'Z': return Desired_Outcome_win;
        default: return Desired_Outcome_invalid;
    }
}


int evaluate2(Move opponent, Desired_Outcome outcome)
{
    assert(outcome != Desired_Outcome_invalid);
    
    Move mine = Move_invalid;
    
    switch (outcome)
    {
        case Desired_Outcome_win:
        {
            switch (opponent)
            {
                case Move_rock: mine = Move_paper; break;
                case Move_paper: mine = Move_scissors; break;
                case Move_scissors: mine = Move_rock; break;
            }
        } break;
        case Desired_Outcome_lose:
        {
            switch (opponent)
            {
                case Move_rock: mine = Move_scissors; break;
                case Move_paper: mine = Move_rock; break;
                case Move_scissors: mine = Move_paper; break;
            }
            
        } break;
        case Desired_Outcome_draw:
        {
            mine = opponent;
        } break;
    }
    
    return evaluate(mine, opponent);
}

PART1()
{
    String_View file_contents = read_entire_file("../data/day2.txt", &arena);
    
    String_View line;
    
    int total_score = 0;
    int i = 0;
    
    while (read_line(&file_contents, &line))
    {
        Move my_move = get_move(*line.end);
        Move opponent_move = get_move(*line.begin);
        
        total_score += evaluate(my_move, opponent_move);
        i++;
    }
    
    printf("%d\n", total_score);
}

PART2()
{
    String_View file_contents = read_entire_file("../data/day2.txt", &arena);
    
    String_View line;
    
    int total_score = 0;
    int i = 0;
    
    while (read_line(&file_contents, &line))
    {
        Desired_Outcome desired_outcome  = get_desired_outcome(*line.end);
        Move opponent_move = get_move(*line.begin);
        
        total_score += evaluate2(opponent_move, desired_outcome);
        i++;
    }
    
    printf("%d\n", total_score);
}