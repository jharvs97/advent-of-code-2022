#include "aoc.cpp"

void run()
{
    HMODULE library = LoadLibraryA("aoc.dll");
    
    if (library)
    {
        aoc_func* part1 = (aoc_func*) GetProcAddress(library, MACRO_TO_STRING(PART1_NAME));
        aoc_func* part2 = (aoc_func*) GetProcAddress(library, MACRO_TO_STRING(PART2_NAME));
        
        if (part1 && part2)
        {
            Memory_Arena arena = arena_create(1024 * 1024 * 32);
            part1(arena);
            part2(arena);
        }
        else
        {
            printf("Couldn't load AOC functions\n");
        }
    }
    else
    {
        printf("Couldn't find aoc.dll\n");
    }
}

void change_working_dir_to_exe_location()
{
    wchar_t module_file_name[MAX_PATH];
    GetModuleFileNameW(0, module_file_name, MAX_PATH);
    
    PathCchRemoveFileSpec(module_file_name, MAX_PATH);
    
    SetCurrentDirectoryW(module_file_name);
}

void main()
{
    change_working_dir_to_exe_location();
    run();
}