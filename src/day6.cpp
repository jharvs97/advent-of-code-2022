#include "aoc.cpp"

// ASCII alphabet character to number between 0-25
usize char_to_alpha_index(char c)
{
    return c - 'a';
}

u32 char_to_bitmask(char c)
{
    return 1ul << char_to_alpha_index(c);
}

s32 get_index_of_longest_substring_of_distinct_characters(String_View data, u32 num_of_distinct_characters)
{
    u32 begin, end;
    bool found = false;
    
    for (begin = 0, end = num_of_distinct_characters;
         (data.begin + end) <= data.end;
         begin++, end++)
    {
        u32 bitmask = 0;
        for (u32 i = begin;
             i < end;
             i++)
        {
            bitmask |= char_to_bitmask(data.begin[i]);
        }
        
        if (__popcnt(bitmask) == num_of_distinct_characters)
        {
            found = true;
            break;
        }
    }
    
    return found ? (s32) end : -1;
}

s32 my_shit_solution(String_View data, u32 num)
{
    u32 counts[26] = {};
    u32 bitfield = 0;
    bool found = false;
    
    for (usize i = 0;
         i < num;
         i++)
    {
        char char_to_add = data.begin[i];
        bitfield |= char_to_bitmask(char_to_add);
        
        u32 count_before_add = counts[char_to_alpha_index(char_to_add)];
        counts[char_to_alpha_index(char_to_add)] = 1ul << (32 - _lzcnt_u32(count_before_add));
    }
    
    u32 begin, end;
    for (begin = 0, end = num;
         data.begin + end <= data.end;
         begin++, end++)
    {
        if (__popcnt(bitfield) == num) [[unlikely]]
        {
            found = true;
            break;
        }
        
        char char_to_remove = data.begin[begin];
        
        counts[char_to_alpha_index(char_to_remove)] >>= 1;
        u32 count_of_removed = counts[char_to_alpha_index(char_to_remove)];
        
        bool is_count_not_zero = (bool) count_of_removed;
        u32 mask = ~(char_to_bitmask(char_to_remove)) | (((u32) is_count_not_zero) << char_to_alpha_index(char_to_remove));
        
        bitfield &= mask;
        
        char char_to_add = data.begin[end];
        bitfield |= char_to_bitmask(char_to_add);
        
        u32 count_before_add = counts[char_to_alpha_index(char_to_add)];
        
        counts[char_to_alpha_index(char_to_add)] = 1ul << (32 - _lzcnt_u32(count_before_add));
    }
    
    return found ? (s32) end : -1;
}

s32 benny_solution(String_View data, u32 num)
{
    u32 filter = 0;
    
    for (usize i = 0;
         i < num-1;
         i++)
    {
        char c = data.begin[i];
        filter ^= (1ul << (c - 'a'));
    }
    
    u64 begin, end;
    for (begin = 0, end = num-1;
         data.begin + end <= data.end;
         begin++, end++)
    {
        char first = data.begin[begin];
        char last = data.begin[end];
        filter ^= (1ul << (last - 'a'));
        bool res = __popcnt(filter) == num;
        filter ^= (1ul << (first - 'a'));
        if (res)
        {
            return (s32) end + 1;
        }
    }
    
    return -1;
}


s32 primagen_solution(String_View data, int num_of_distinct_characters)
{
    u32 begin, end;
    u32 bitmask = 0;
    
    for (begin = 0, end = num_of_distinct_characters;
         (data.begin + end) <= data.end;
         begin++, end++)
    {
        bitmask = 0;
        for (u32 i = begin;
             i < end;
             i++)
        {
            u32 prev = bitmask;
            bitmask |= char_to_bitmask(data.begin[i]);
            if (prev == bitmask)
            {
                goto end_of_loop;
            }
        }
        
        return end;
        
        end_of_loop:
        continue;
    }
    
    return -1;
}

PART1()
{
    String_View data = read_entire_file("../data/day6.txt", &arena);
    my_shit_solution(data, 4);
    
    /*printf("%ld\n", result);*/
}

PART2()
{
    String_View data = read_entire_file("../data/long.txt", &arena);
    
    LARGE_INTEGER large_int;
    /*
    {
        QueryPerformanceCounter(&large_int);
        u64 ticks_before = large_int.QuadPart;
        
        s32 result = get_index_of_longest_substring_of_distinct_characters(data, 23);
        
        QueryPerformanceCounter(&large_int);
        u64 ticks_after = large_int.QuadPart;
        
        u64 difference = ticks_after - ticks_before;
        printf("get_index returned %ld and took %lld ms\n", result, difference/10000ull);
    }
    */
    {
        QueryPerformanceCounter(&large_int);
        u64 ticks_before = large_int.QuadPart;
        s32 result = my_shit_solution(data, 23);
        QueryPerformanceCounter(&large_int);
        u64 ticks_after = large_int.QuadPart;
        
        u64 difference = ticks_after - ticks_before;
        printf("my solution returned %ld and took %lld ms\n", result, difference/10000ull);
    }
    
    {
        QueryPerformanceCounter(&large_int);
        u64 ticks_before = large_int.QuadPart;
        s32 result = benny_solution(data, 23);
        QueryPerformanceCounter(&large_int);
        u64 ticks_after = large_int.QuadPart;
        
        u64 difference = ticks_after - ticks_before;
        printf("benny's solution returned %ld and took %lld ms\n", result, difference/10000ull);
    }
}
