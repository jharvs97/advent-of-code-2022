#include "aoc.cpp"

struct Assignment
{
    u32 lower, upper; 
};

struct Sorted_Assignments
{
    Assignment larger, smaller;
};

Sorted_Assignments sort_assignemnts(Assignment a, Assignment b)
{
    u32 a_diff = a.upper - a.lower;
    u32 b_diff = b.upper - b.lower;
    
    Assignment larger, smaller;
    
    if (a_diff > b_diff)
    {
        larger = a;
        smaller = b;
    }
    else
    {
        larger = b;
        smaller = a;
    }
    
    return {larger, smaller};
}

Assignment parse_assignment(String_View parse_string, Memory_Arena arena)
{
    String_View first_num_sv = string_view_split(&parse_string, '-');
    String_View second_num_sv = parse_string;
    
    u32 lower = (u32) string_view_to_int(first_num_sv, arena);
    u32 upper = (u32) string_view_to_int(second_num_sv, arena);
    
    return {lower, upper};
}

bool assignments_entirely_overlap(Sorted_Assignments assignments)
{
    Assignment larger = assignments.larger;
    Assignment smaller = assignments.smaller;
    return (larger.lower <= smaller.lower) && (larger.upper >= smaller.upper);
}

bool assignments_overlap(Sorted_Assignments assignments)
{
    Assignment larger = assignments.larger;
    Assignment smaller = assignments.smaller;
    
    return 
    ((smaller.upper >= larger.lower) && (smaller.lower <= larger.lower)) || 
    ((smaller.lower <= larger.upper) && (smaller.upper >= larger.upper)) ||
    ((larger.lower <= smaller.lower) && (larger.upper >= smaller.upper));
}

PART1()
{
    String_View file_contents = read_entire_file("../data/day4.txt", &arena);
    String_View line;
    
    u32 overlap_count = 0;
    
    while (read_line(&file_contents, &line))
    {
        String_View first = string_view_split(&line, ',');
        String_View second = string_view_split(&line, '\n');
        
        Assignment first_assignment = parse_assignment(first, arena);
        Assignment second_assignment = parse_assignment(second, arena);
        
        Sorted_Assignments assignments = sort_assignemnts(first_assignment, second_assignment);
        
        if (assignments_entirely_overlap(assignments))
        {
            overlap_count++;
        }
    }
    
    printf("%d\n", overlap_count);
}

PART2()
{
    String_View file_contents = read_entire_file("../data/day4.txt", &arena);
    String_View line;
    
    u32 overlap_count = 0;
    
    while (read_line(&file_contents, &line))
    {
        String_View first = string_view_split(&line, ',');
        String_View second = string_view_split(&line, '\n');
        
        Assignment first_assignment = parse_assignment(first, arena);
        Assignment second_assignment = parse_assignment(second, arena);
        
        Sorted_Assignments assignments = sort_assignemnts(first_assignment, second_assignment);
        
        if (assignments_overlap(assignments))
        {
            overlap_count++;
        }
    }
    
    printf("%d\n", overlap_count);
}