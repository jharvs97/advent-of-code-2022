#include "aoc.cpp"

#define MAX_STACK_SIZE 128
struct Crate_Stack
{
    char items[MAX_STACK_SIZE];
    usize next_open_slot;
};

#define MAX_CRATE_STACK_SIZE 128
struct Crates
{
    Crate_Stack crates[MAX_CRATE_STACK_SIZE];
};

struct Command
{
    u32 num_to_move;
    usize source_index;
    usize dest_index;
};

#define MAX_COMMANDS 2048
struct Command_Buffer
{
    Command* buffer;
    usize size;
};

struct Parse_Result
{
    Crates* crates;
    Command_Buffer commands;
};

enum Crate_Parse_Result
{
    Crate_Parse_Result_ok,
    Crate_Parse_Result_done,
};

enum Parse_State
{
    Parse_State_none = 0,
    Parse_State_crates,
    Parse_State_commands,
};

void stack_push(Crate_Stack* s, char c)
{
    assert(s->next_open_slot != (MAX_STACK_SIZE - 1));
    s->items[s->next_open_slot] = c;
    s->next_open_slot++;
}

char stack_pop(Crate_Stack* s)
{
    assert(s->next_open_slot != 0);
    char c = s->items[s->next_open_slot - 1];
    s->next_open_slot--;
    
    return c;
}

char stack_peek(Crate_Stack *s)
{
    return s->items[s->next_open_slot - 1];
}

bool stack_is_empty(Crate_Stack s)
{
    return s.next_open_slot == 0;
}

void reverse_crate_stacks(Crates *crates)
{
    for (usize i = 0;
         i < MAX_CRATE_STACK_SIZE;
         i++)
    {
        Crate_Stack stack = crates->crates[i];
        Crate_Stack reversed = {};
        while (!stack_is_empty(stack))
        {
            char item = stack_pop(&stack);
            stack_push(&reversed, item);
        }
        
        crates->crates[i] = reversed;
    }
}

Crate_Parse_Result parse_crates_line(Crates* crates, String_View line)
{
    int i = 0;
    for (char* c = line.begin + 1;
         c <= line.end;
         c += 4, i++)
    {
        if (*c >= '0' && *c <= '9')
        {
            return Crate_Parse_Result_done;
        }
        
        if (*c != ' ')
        {
            stack_push(&crates->crates[i], *c);
        }
    }
    
    return Crate_Parse_Result_ok;
}

void command_buffer_push(Command_Buffer* commands, Command command)
{
    assert(commands->size < MAX_COMMANDS);
    commands->buffer[commands->size++] = command;
}

void parse_command(Command_Buffer *commands, String_View line, Memory_Arena arena)
{
    // Skip 'move'
    string_view_split(&line, ' ');
    
    u32 num_to_move = (u32) string_view_to_int(string_view_split(&line, ' '), arena);
    
    // Skip 'from'
    string_view_split(&line, ' ');
    
    // Subtract one to get an array index
    usize source_index = (usize) (string_view_to_int(string_view_split(&line, ' '), arena) - 1);
    
    // Skip 'to'
    string_view_split(&line, ' ');
    
    // Subtract one to get an array index
    usize dest_index = (usize) (string_view_to_int(string_view_split(&line, '\n'), arena) - 1);
    
    Command command = {};
    command.num_to_move = num_to_move;
    command.source_index = source_index;
    command.dest_index = dest_index;
    
    command_buffer_push(commands, command);
}

void do_command_crate_mover_9000(Command command, Crates* crates)
{
    Crate_Stack *source = &crates->crates[command.source_index];
    Crate_Stack *dest   = &crates->crates[command.dest_index];
    
    for (u32 i = 0;
         i < command.num_to_move;
         i++)
    {
        char item = stack_pop(source);
        stack_push(dest, item);
    }
}

char* evaluate_commands_crate_mover_9000(Command_Buffer commands, Crates* crates, Memory_Arena* arena)
{
    for (usize i = 0;
         i < commands.size;
         i++)
    {
        Command command = commands.buffer[i];
        do_command_crate_mover_9000(command, crates);
    }
    
    char* out_buffer = arena_push_array(arena, char, MAX_CRATE_STACK_SIZE + 1);
    
    for (usize i = 0;
         i < MAX_CRATE_STACK_SIZE;
         i++)
    {
        out_buffer[i] = stack_peek(&crates->crates[i]);
    }
    
    out_buffer[MAX_CRATE_STACK_SIZE] = 0;
    
    return out_buffer;
}

void do_command_crate_mover_9001(Command command, Crates* crates)
{
    Crate_Stack *source = &crates->crates[command.source_index];
    Crate_Stack *dest   = &crates->crates[command.dest_index];
    
    Crate_Stack temp = {};
    for (u32 i = 0;
         i < command.num_to_move;
         i++)
    {
        char item = stack_pop(source);
        stack_push(&temp, item);
    }
    
    for (u32 i = 0;
         i < command.num_to_move;
         i++)
    {
        char item = stack_pop(&temp);
        stack_push(dest, item);
    }
}

char* evaluate_commands_crate_mover_9001(Command_Buffer commands, Crates* crates, Memory_Arena* arena)
{
    for (usize i = 0;
         i < commands.size;
         i++)
    {
        Command command = commands.buffer[i];
        do_command_crate_mover_9001(command, crates);
    }
    
    char* out_buffer = arena_push_array(arena, char, MAX_CRATE_STACK_SIZE + 1);
    
    for (usize i = 0;
         i < MAX_CRATE_STACK_SIZE;
         i++)
    {
        out_buffer[i] = stack_peek(&crates->crates[i]);
    }
    
    out_buffer[MAX_CRATE_STACK_SIZE] = 0;
    
    return out_buffer;
}

Parse_Result parse_file(Memory_Arena *arena)
{
    String_View file_contents = read_entire_file("../data/day5.txt", arena);
    String_View line;
    Crates* crates = (Crates*) arena_allocate(arena, sizeof(Crates));
    
    Parse_State state = Parse_State_crates;
    
    Command_Buffer commands = {};
    commands.buffer = arena_push_array(arena, Command, MAX_COMMANDS);
    
    while (read_line(&file_contents, &line))
    {
        switch (state)
        {
            case Parse_State_crates:
            {
                if (!string_view_is_empty(line))
                {
                    Crate_Parse_Result result = parse_crates_line(crates, line);
                    
                    if (result == Crate_Parse_Result_done)
                    {
                        reverse_crate_stacks(crates);
                        state = Parse_State_commands;
                    } 
                }
            } break;
            case Parse_State_commands:
            {
                if (!string_view_is_empty(line))
                {
                    parse_command(&commands, line, *arena);
                }
            } break;
        }
    }
    
    return {crates, commands};
}

PART1()
{
    Parse_Result result = parse_file(&arena);
    char* output = evaluate_commands_crate_mover_9000(result.commands, result.crates, &arena);
    
    printf("%s\n", output);
}

PART2()
{
    Parse_Result result = parse_file(&arena);
    char* output = evaluate_commands_crate_mover_9001(result.commands, result.crates, &arena);
    
    printf("%s\n", output);
}