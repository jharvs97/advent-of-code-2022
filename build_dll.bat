@echo off

if not exist build mkdir build

set name=day7
set file=%name%.cpp

pushd build

del day*

cl /DBUILD_DLL /W4 /LDd /EHsc /Oi /Od /Zi /FC /std:c++20 ..\src\%file% /link /incremental:no /OUT:aoc.dll 

popd