@echo off

if not exist build mkdir build

pushd build

cl /EHsc /W4 /Oi /O2 /Zi /FC /Fe"gen_long_string.exe" ..\src\gen_long_string.cpp /link /incremental:no 

popd