@echo off

if not exist build mkdir build

pushd build

cl /EHsc /W4 /Oi /O2 /Zi /FC /Fe"aoc.exe" /std:c++20 ..\src\main.cpp /link /incremental:no kernel32.lib pathcch.lib

popd